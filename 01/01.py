# 2020-12-01. Advent of Code 2020, Day 01.

nums = []

with open("input.txt", "r") as f:
    for line in f:
        n = int(line.strip())
        nums.append(n)

# Part A
for i in nums:
    if 2020 - i in nums:
        print(i, "and", 2020 - i, "sum to 2020, with product", i * (2020 - i))
        break

# Part B
for i in range(len(nums)):
    for j in range(i, len(nums)):
        for k in range(j, len(nums)):
            if nums[i] + nums[j] + nums[k] == 2020:
                print(nums[i], "and", nums[j], "and", nums[k], "sum to 2020, with product", nums[i] * nums[j] * nums[k])
