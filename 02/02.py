# 2020-12-02. Advent of Code 2020, Day 02.
import re

validA = 0
validB = 0

with open("input.txt", "r") as f:
    for line in f:
        lowLim = int(re.search("\d+-", line).group()[:-1])
        highLim = int(re.search("-\d+", line).group()[1:])
        c = re.search("\w:", line).group()[0]
        pwd = re.search(": \w*", line).group()[2:]
        # print(lowLim,"-",highLim," ",chr,":",pwd)

        # Part A
        # there must be between lowLim and highLim (inclusive) occurrences of the key character to be valid
        num = len(re.findall(c, pwd))
        if lowLim <= num <= highLim:
            validA += 1

        # Part B
        # lowLim and highLim describe two 1-indexed positions: exactly one must contain key character to be valid
        if (pwd[lowLim - 1] == c) ^ (pwd[highLim - 1] == c):
            validB += 1

print("Part A:", validA, "valid passwords")
print("Part B:", validB, "valid passwords")
