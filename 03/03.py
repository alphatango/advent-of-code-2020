# 2020-12-03. Advent of Code 2020, Day 03.

grid = []
with open("input.txt", "r") as f:
    for line in f:
        newRow = []
        for c in line.strip():
            newRow.append(c)
        grid.append(newRow)
height = len(grid)
width = len(grid[0])


def get_trees_on_slope(slopeX, slopeY):
    posX = posY = 0
    treeCount = 0
    while posY < height:
        if grid[posY][posX] == "#":
            treeCount += 1
        posX = (posX + slopeX) % width
        posY += slopeY
    return treeCount


# Part A
print("Part A: number of trees is", get_trees_on_slope(3,1))

# Part B
print("Part B: product of number of trees on given slopes is:",
      get_trees_on_slope(1, 1) *
      get_trees_on_slope(3, 1) *
      get_trees_on_slope(5, 1) *
      get_trees_on_slope(7, 1) *
      get_trees_on_slope(1, 2)
    )