# 2020-12-04. Advent of Code 2020, Day 04.
import re

# read data
passports = []
with open("input.txt", "r") as f:
    p = {}
    for line in f:
        if line == "\n":
            passports.append(p)
            p = {}
        else:
            lstFields = line.split()
            for field in lstFields:
                k, v = field.split(":")
                p[k] = v
    passports.append(p)

# Part A
count = 0
for p in passports:
    if len(p) == 8 or (len(p) == 7 and "cid" not in p):
        count += 1
print("Part A: number of valid passports:", count)

# Part B
count = 0
for p in passports:
    if not (len(p) == 8 or (len(p) == 7 and "cid" not in p)):
        continue
    if re.fullmatch("\d{4}", p["byr"]) is None or \
            (int(p["byr"]) < 1920 or int(p["byr"]) > 2002):
        continue
    if re.fullmatch("\d{4}", p["iyr"]) is None or \
            (int(p["iyr"]) < 2010 or int(p["iyr"]) > 2020):
        continue
    if re.fullmatch("\d{4}", p["eyr"]) is None or \
            (int(p["eyr"]) < 2020 or int(p["eyr"]) > 2030):
        continue
    if re.fullmatch("\d+(in|cm)", p["hgt"]) is None or \
            (p["hgt"][-2:] == "in" and (int(p["hgt"][:-2]) < 59 or int(p["hgt"][:-2]) > 76)) or \
            (p["hgt"][-2:] == "cm" and (int(p["hgt"][:-2]) < 150 or int(p["hgt"][:-2]) > 193)):
        continue
    if re.fullmatch("#[\dabcdef]{6}", p["hcl"]) is None:
        continue
    if re.fullmatch("amb|blu|brn|gry|grn|hzl|oth", p["ecl"]) is None:
        continue
    if re.fullmatch("\d{9}", p["pid"]) is None:
        continue
    count += 1
print("Part B: number of valid passports:", count)
