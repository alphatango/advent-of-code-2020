# 2020-12-05. Advent of Code 2020, Day 05.

# read data
seats = []
with open("input.txt", "r") as f:
    for line in f:
        seats.append(line.strip())

# def do_binary_split(s, lowNum, highNum, lowSplitChr, highSplitChr):
#     """
#     Conducts repeated binary splits on a numeric range based on characters in a string.
#     Does not check that parameters given will properly split to completion but throws an AssertionError if not.
#     :param s: string of characters to determine the split
#     :param lowNum: low end of numeric range to be split
#     :param highNum: high end of numeric range to be split
#     :param lowSplitChr: character in string indicating that the lower half of the numeric range is to be kept
#     :param highSplitChr: character in string indicating that the higher half of the numeric range is to be kept
#     :return: integer between lowNum and highNum (inclusive)
#     """
#     for c in s:
#         if c == lowSplitChr:
#             highNum = highNum - (highNum - lowNum + 1) / 2
#         elif c == highSplitChr:
#             lowNum = lowNum + (highNum - lowNum + 1) / 2
#     assert(lowNum == highNum)
#     return int(lowNum)
#
#
# def get_seat_id(seat):
#     rowStr = seat[:7]
#     colStr = seat[7:]
#     row = do_binary_split(rowStr, 0, 127, "F", "B")
#     col = do_binary_split(colStr, 0, 7, "L", "R")
#     return row * 8 + col


# a (much) better implementation
def get_seat_id(seat):
    charToBin = {'B': '1', 'F': '0',
                'L': '0', 'R': '1'}
    binSeat = ''.join(charToBin[c] for c in seat)  # string of binary digits
    return int(binSeat, base=2)


# confirm test seat strings given in puzzle
assert(get_seat_id("BFFFBBFRRR") == 567)
assert(get_seat_id("FFFBBBFRRR") == 119)
assert(get_seat_id("BBFFBBFRLL") == 820)

# create set of seat IDs on boarding passes
lstID = {get_seat_id(s) for s in seats}

# Part A
maxSeatID = max(lstID)
print("Highest seat ID:", maxSeatID)

# Part B
minSeatID = min(lstID)
allSeats = set(range(minSeatID, maxSeatID))
mySeatID = allSeats - lstID  # should be a singleton set
print("My seat ID is:", list(mySeatID)[0])
