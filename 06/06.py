# 2020-12-06. Advent of Code 2020, Day 06.
import string

anyYes = []
allYes = []
hasAnyYes = set()
hasAllYes = set(string.ascii_lowercase)
with open("input.txt", "r") as f:
    for line in f:
        if line == "\n":
            anyYes.append(hasAnyYes)
            allYes.append(hasAllYes)
            hasAnyYes = set()
            hasAllYes = set(string.ascii_lowercase)
        else:
            c = set(line.strip())
            hasAnyYes.update(c)
            hasAllYes = hasAllYes.intersection(c)
    allYes.append(hasAllYes)
    anyYes.append(hasAnyYes)

print("Sum over groups of (# positively answered by at least one person in the group):", sum(len(g) for g in anyYes))
print("Sum over groups of (# positively answered by all people in the group):", sum(len(g) for g in allYes))