# 2020-12-08. Advent of Code 2020, Day 07.
import re
import networkx as nx

g = nx.DiGraph()
with open("input.txt", "r") as f:
    for line in f:
        containerBag = re.match(".*(?= bags contain)", line).group()
        g.add_node(containerBag)
        if re.search("no other", line) is None:
            for b in re.finditer("\d+.*?(?= bags?)", line):
                subBagQuantity = int(re.match("\d+", b.group()).group())
                subBagColour = re.search("[a-z][a-z\s]*", b.group()).group()
                g.add_edge(containerBag, subBagColour, weight=subBagQuantity)

# Part A
allContainers = set()
toAdd = {"shiny gold"}
while len(toAdd) > 0:
    b = toAdd.pop()
    if b not in allContainers:
        allContainers.add(b)
        for c in g.predecessors(b):
            toAdd.add(c)
print("There are", len(allContainers) - 1, "possible outermost containers for a shiny gold bag")

# Part B
bagCount = {}
toAdd = {"shiny gold": 1}
while len(toAdd) > 0:
    b = toAdd.popitem()
    if b[0] in bagCount:
        bagCount[b[0]] += b[1]
    else:
        bagCount[b[0]] = b[1]
    for subBag in g.successors(b[0]):
        if subBag in toAdd:
            toAdd[subBag] += b[1] * g.get_edge_data(b[0], subBag)["weight"]
        else:
            toAdd[subBag] = b[1] * g.get_edge_data(b[0], subBag)["weight"]
print("There are", sum(bagCount.values()) - 1, "bags inside every shiny gold bag")

