# 2020-12-08. Advent of Code 2020, Day 08.

instructions = {}
with open("input.txt", "r") as f:
    count = 0
    for line in f:
        tmp = line.strip().split()
        instructions[count] = [tmp[0], int(tmp[1])]
        count += 1
numInst = len(instructions)


def do_step(pointer, accumulator):
    if instructions[pointer][0] == "nop":
        return pointer + 1, accumulator
    if instructions[pointer][0] == "acc":
        return pointer + 1, accumulator + instructions[pointer][1]
    if instructions[pointer][0] == "jmp":
        return pointer + instructions[pointer][1], accumulator


def run_to_loop_or_termination():
    pointer = accumulator = 0
    executed = set()
    while pointer not in executed and pointer < numInst:
        executed.add(pointer)
        pointer, accumulator = do_step(pointer, accumulator)
    return pointer, accumulator


def swap_jmp_nop(inst, instNumToSwap):
    if inst[instNumToSwap][0] == "jmp":
        inst[instNumToSwap][0] = "nop"
    elif inst[instNumToSwap][0] == "nop":
        inst[instNumToSwap][0] = "jmp"


# Part A
ptr, acc = run_to_loop_or_termination()
print("Accumulator value before executing any instruction a second time:", acc)

# Part B
instToChange = 0
while instToChange < numInst:
    swap_jmp_nop(instructions, instToChange)
    ptr, acc = run_to_loop_or_termination()
    if ptr == numInst:
        print("Changing instruction", instToChange, "to", instructions[instToChange][0],
              "led to program termination with accumulator value", acc)
        break
    else:
        swap_jmp_nop(instructions, instToChange)
        instToChange += 1
