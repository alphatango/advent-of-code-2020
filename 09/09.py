# 2020-12-09. Advent of Code 2020, Day 09.
import itertools

PREAMBLE_LENGTH = 25
lstNum = []
with open("input.txt", "r") as f:
    for line in f:
        lstNum.append(int(line.strip()))

# Part A
i = PREAMBLE_LENGTH
valid = True
while i < len(lstNum) and valid:
    valid = False
    previousNums = set(lstNum[i-PREAMBLE_LENGTH:i])  # set because summands must be different
    for c in itertools.combinations(previousNums, 2):
        if sum(c) == lstNum[i]:
            valid = True
            i += 1
            break
if i == len(lstNum):
    print("All numbers in list are valid")
    invalidNum = None
else:
    print("Number", lstNum[i], "at index", i, "is not valid")
    invalidNum = lstNum[i]

# Part B
rangeFound = False
startIndex = 0
while not rangeFound and startIndex < len(lstNum):
    total = lstNum[startIndex] + lstNum[startIndex+1]
    i = 1
    while total < invalidNum:
        i += 1
        total += lstNum[startIndex+i]
    if total == invalidNum:
        rangeFound = True
        print("Contiguous range found from index", startIndex, "to", startIndex + i)
    else:
        startIndex += 1
if not rangeFound:
    print("Couldn't find a contiguous range summing to", invalidNum)
else:
    contiguousList = lstNum[startIndex:startIndex+i+1]
    print("Sum of smallest and largest numbers in this range:", min(contiguousList) + max(contiguousList))
