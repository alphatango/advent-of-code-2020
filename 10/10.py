# 2020-12-11. Advent of Code 2020, Day 10.
from collections import Counter

adapters = []
with open("input.txt", "r") as f:
    for line in f:
        if line != "":
            adapters.append(int(line.strip()))
adapters.append(0)  # power socket
adapters.append(max(adapters) + 3)  # device
adapters.sort()

# Part A
diff = [adapters[i + 1] - adapters[i] for i in range(len(adapters) - 1)]
stepFreq = Counter()
for i in diff:
    stepFreq[i] += 1
print("Number of 1J steps multiplied by number of 3J steps:", stepFreq[1] * stepFreq[3])

# Part B
# by inspection, only differences of 1J and 3J
# create a list of size of 1J chains
s = "".join(str(d) for d in diff)
chainSizes = [len(c) for c in s.split("3")]
# by inspection, chains are of size 0-4 1J steps
# number of compositions of integer n where size of each partition is (strictly) 0<p<4
# this represents the number of ways within each chain of 1J steps of connecting up the adapters
# (probably a general soln to this somewhere...)
compositionMap = {0: 1,  # no steps (just 3J jumps on either side)
                  1: 1,  # 1
                  2: 2,  # 1+1 or 2
                  3: 4,  # 1+1+1 or 1+2 or 2+1 or 3
                  4: 7  # 1+1+1+1 or 1+1+2 or 1+2+1 or 2+1+1 or 1+3 or 3+1 or 2+2
                  }
total = 1
for i in chainSizes:
    total *= compositionMap[i]
print("Total number of ways to connect adapters:", total)
