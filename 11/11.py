# 2020-12-12. Advent of Code 2020, Day 11.


class Lounge:
    def __init__(self, mode="adj", limit=4):
        self.grid = {}  # keys = seat coords (x, y); values = occupied (True) or not (False)
        self.adjSeats = {}  # for each seat, list of <=8 adj seat coords (x,y)
        self.LOSSeats = {}  # for each seat, list of LOS seat coords in each direction (x,y)
        self.mode = mode
        self.limit = limit
        with open("input.txt", "r") as f:
            self.maxCol = 0
            rowNum = 0
            for line in f:
                colNum = 0
                for ch in line.strip():
                    if ch == "L":  # unoccupied seat
                        self.grid[(rowNum, colNum)] = False
                    elif ch == "#":  # occupied seat
                        self.grid[(rowNum, colNum)] = True
                    colNum += 1
                rowNum += 1
        self.maxRow = rowNum - 1
        self.maxCol = colNum - 1
        for c in self.grid:
            self.adjSeats[c] = self.get_adj_seats(c)
            self.LOSSeats[c] = self.get_LOS_seats(c)

    def get_num_filled_relevant_seats(self, cell, mode="adj"):
        counter = 0
        if mode == "adj":
            for c in self.adjSeats[cell]:
                if self.grid[c]:
                    counter += 1
        elif mode == "LOS":
            for c in self.LOSSeats[cell]:
                if self.grid[c]:
                    counter += 1
        return counter

    def get_adj_seats(self, cell):
        lst = []
        for c in [(cell[0] + i, cell[1] + j) for i in (-1, 0, 1) for j in (-1, 0, 1) if i != 0 or j != 0]:
            if c in self.grid:
                lst.append(c)
        return lst

    def get_LOS_seats(self, cell):
        lst = []
        for step in [(i, j) for i in (-1, 0, 1) for j in (-1, 0, 1) if i != 0 or j != 0]:
            found = False
            currentCell = cell
            while not found:
                currentCell = (currentCell[0] + step[0], currentCell[1] + step[1])
                if currentCell in self.grid:
                    lst.append(currentCell)
                    found = True
                elif currentCell[0] < 0 or currentCell[0] > self.maxRow or \
                        currentCell[1] < 0 or currentCell[1] > self.maxCol:
                    found = True
        return lst

    def do_step(self):
        # returns True if grid changed, False if static
        changed = False
        newGrid = {}
        for c in self.grid:
            if not self.grid[c]:  # empty seat, becomes occupied if no adj occupied seats
                if self.get_num_filled_relevant_seats(c, mode=self.mode) == 0:
                    newGrid[c] = True
                    changed = True
                else:
                    newGrid[c] = False
            elif self.grid[c]:
                # occupied seat, becomes empty if 4+ adj seats are occupied (Part A)
                # or 5+ LOS seats are occupied (Part B)
                if self.get_num_filled_relevant_seats(c, mode=self.mode) >= self.limit:
                    newGrid[c] = False
                    changed = True
                else:
                    newGrid[c] = True
        self.grid = newGrid
        return changed

    def evolve(self):
        changed = True
        while changed:
            changed = self.do_step()

    def get_num_occupied_seats(self):
        return sum(self.grid.values())


# Part A
lounge = Lounge(mode="adj", limit=4)
lounge.evolve()
print("Part A -- Number of occupied seats when static:", lounge.get_num_occupied_seats())

# Part B
lounge = Lounge(mode="LOS", limit=5)
lounge.evolve()
print("Part B -- Number of occupied seats when static:", lounge.get_num_occupied_seats())
