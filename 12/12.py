# 2020-12-12. Advent of Code 2020, Day 12.
from enum import Enum


class Direction(Enum):
    N = 0
    E = 1
    S = 2
    W = 3


class Spaceship:
    def __init__(self):
        self.inst = []
        with open("input.txt", "r") as f:
            for line in f:
                i = line[0]
                num = int(line[1:].strip())
                self.inst.append((i, num))
        self.currentPos = (0, 0)
        self.waypoint = (10, 1)
        self.currentDir = Direction.E

    def rotate(self, orientation, degrees):
        if orientation == "R":
            self.currentDir = Direction((self.currentDir.value + degrees // 90) % 4)
        elif orientation == "L":
            self.currentDir = Direction((self.currentDir.value - degrees // 90) % 4)

    def rotate_waypoint(self, orientation, degrees):
        if (orientation == "R" and degrees == 90) or (orientation == "L" and degrees == 270):
            self.waypoint = (self.waypoint[1], -self.waypoint[0])
        elif (orientation == "L" and degrees == 90) or (orientation == "R" and degrees == 270):
            self.waypoint = (-self.waypoint[1], self.waypoint[0])
        elif degrees == 180:
            self.waypoint = (-self.waypoint[0], -self.waypoint[1])

    def move(self, dist, direction=None):
        if direction is None:
            direction = self.currentDir
        if direction == Direction.N:
            self.currentPos = (self.currentPos[0], self.currentPos[1] + dist)
        elif direction == Direction.S:
            self.currentPos = (self.currentPos[0], self.currentPos[1] - dist)
        elif direction == Direction.E:
            self.currentPos = (self.currentPos[0] + dist, self.currentPos[1])
        elif direction == Direction.W:
            self.currentPos = (self.currentPos[0] - dist, self.currentPos[1])

    def move_waypoint(self, dist, direction=None):
        if direction is None:
            direction = self.currentDir
        if direction == Direction.N:
            self.waypoint = (self.waypoint[0], self.waypoint[1] + dist)
        elif direction == Direction.S:
            self.waypoint = (self.waypoint[0], self.waypoint[1] - dist)
        elif direction == Direction.E:
            self.waypoint = (self.waypoint[0] + dist, self.waypoint[1])
        elif direction == Direction.W:
            self.waypoint = (self.waypoint[0] - dist, self.waypoint[1])

    def execute_inst_spaceship_only(self, instruction):
        if instruction[0] == "N":
            self.move(direction=Direction.N, dist=instruction[1])
        elif instruction[0] == "E":
            self.move(direction=Direction.E, dist=instruction[1])
        elif instruction[0] == "S":
            self.move(direction=Direction.S, dist=instruction[1])
        elif instruction[0] == "W":
            self.move(direction=Direction.W, dist=instruction[1])
        elif instruction[0] == "F":
            self.move(dist=instruction[1])
        elif instruction[0] in ["R", "L"]:
            self.rotate(instruction[0], instruction[1])

    def execute_all_spaceship_only(self):
        for i in self.inst:
            self.execute_inst_spaceship_only(i)

    def execute_inst_with_waypoint(self, instruction):
        if instruction[0] == "N":
            self.move_waypoint(direction=Direction.N, dist=instruction[1])
        elif instruction[0] == "E":
            self.move_waypoint(direction=Direction.E, dist=instruction[1])
        elif instruction[0] == "S":
            self.move_waypoint(direction=Direction.S, dist=instruction[1])
        elif instruction[0] == "W":
            self.move_waypoint(direction=Direction.W, dist=instruction[1])
        elif instruction[0] == "F":
            self.currentPos = (self.currentPos[0] + instruction[1] * self.waypoint[0],
                               self.currentPos[1] + instruction[1] * self.waypoint[1])

        elif instruction[0] in ["R", "L"]:
            self.rotate_waypoint(instruction[0], instruction[1])

    def execute_all_with_waypoint(self):
        for i in self.inst:
            self.execute_inst_with_waypoint(i)

    def get_manhattan_distance(self):
        return abs(self.currentPos[0]) + abs(self.currentPos[1])


# Part A
s = Spaceship()
s.execute_all_spaceship_only()
print("Manhattan distance if only spaceship moved:", s.get_manhattan_distance())

# Part B
s2 = Spaceship()
s2.execute_all_with_waypoint()
print("Manhattan distance if moved with waypoint:", s2.get_manhattan_distance())
