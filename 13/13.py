# 2020-12-13. Advent of Code 2020, Day 13.
import math
import itertools

with open("input.txt", "r") as f:
    depTime = int(f.readline())
    line = f.readline().split(",")
    buses = {}
    for i in range(len(line)):
        if line[i] != "x":
            buses[int(line[i])] = -i

# Part A
toWait = {}
for b in buses:
    toWait[b] = b - (depTime % b)
earliestBus = min(toWait, key=toWait.get)
print("Earliest bus:", earliestBus, "- Time to wait:", toWait[earliestBus],
      "- Product:", earliestBus * toWait[earliestBus])

# Part B
# confirm bus numbers are pairwise coprime
# better fill in the rest of this algorithm to account for non-coprime situations later
for pair in itertools.combinations(buses.keys(), 2):
    if math.gcd(pair[0], pair[1]) != 1:
        print(pair[0], "and", pair[1], "not coprime; revise the algorithm")


def chinese_remainder(dic):
    # applies Chinese Remainder Theorem to dictionary of {moduli: remainders}
    total = 0
    # math.prod is new in Python 3.8+
    prod = math.prod(dic.keys())
    for m in dic:
        p = prod // m
        # note the pow(p, -1, mod) syntax to obtain modular multiplicative inverse is only available in Python 3.8+
        total += dic[m] * pow(p, -1, mod=m) * p
    return total % prod


print("Earliest time meeting conditions:", chinese_remainder(buses))
