# 2020-12-15. Advent of Code 2020, Day 14.
import re
import itertools


def set_bit_in_int(value, bit, changeTo):
    if changeTo == 1:
        return value | (1 << bit)
    else:
        return value & ~(1 << bit)


def apply_bitmask(num, mask):
    n = num
    for m in mask:
        n = set_bit_in_int(n, m, mask[m])
    return n


# Part A
mem = {}
currentMask = {}
with open("input.txt", "r") as f:
    for line in f:
        line = line.strip()
        if re.match("mask", line) is not None:
            maskStr = re.search("[01X]+", line).group()
            currentMask = {}
            for i in range(len(maskStr)):
                if maskStr[len(maskStr) - i - 1] != "X":
                    currentMask[i] = int(maskStr[len(maskStr) - i - 1])
        elif re.match("mem", line) is not None:
            d = re.findall("\d+", line)
            memLoc = int(d[0])
            rawVal = int(d[1])
            mem[memLoc] = apply_bitmask(rawVal, currentMask)
print("Sum of all values in memory:", sum(mem.values()))


# Part B
# can merge some of this with Part A code
def apply_alt_bitmask(num, mask):
    n = num
    for m in mask:
        if mask[m] == 1:
            n = set_bit_in_int(n, m, mask[m])
    return n


def get_masked_memory_locations(num, Xpositions):
    lst = []
    Xmask = {}
    for t in itertools.product(*[(0, 1)] * len(Xpositions)):
        for u in range(len(Xpositions)):
            Xmask[Xpositions[u]] = t[u]
        lst.append(apply_bitmask(num, Xmask))
    return lst


mem = {}
currentMask = {}
with open("input.txt", "r") as f:
    for line in f:
        line = line.strip()
        if re.match("mask", line) is not None:
            maskStr = re.search("[01X]+", line).group()
            currentMask = {}
            currentMaskXPos = []
            for i in range(len(maskStr)):
                if maskStr[len(maskStr) - i - 1] != "X":
                    currentMask[i] = int(maskStr[len(maskStr) - i - 1])
                else:
                    currentMaskXPos.append(i)
        elif re.match("mem", line) is not None:
            d = re.findall("\d+", line)
            memLoc = apply_alt_bitmask(int(d[0]), currentMask)
            rawVal = int(d[1])
            for maskedMem in get_masked_memory_locations(memLoc, currentMaskXPos):
                mem[maskedMem] = rawVal
print("Sum of all values in memory:", sum(mem.values()))
