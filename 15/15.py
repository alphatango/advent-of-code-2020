# 2020-12-15. Advent of Code 2020, Day 15.

with open("input.txt", "r") as f:
    lstNum = [int(n) for n in f.readline().strip().split(",")]
    numStarting = len(lstNum)
    turnSpoken = {}
    for i in range(numStarting):
        turnSpoken[lstNum[i]] = i + 1
    lastNum = lstNum[-1]

for i in range(numStarting, 30000001):
    if lastNum in turnSpoken:
        nextNum = i - turnSpoken[lastNum]
    else:
        nextNum = 0
    turnSpoken[lastNum] = i
    if i == 2020:
        print("2020th number spoken is:", lastNum)
    elif i == 30000000:
        print("30000000th number spoken is:", lastNum)
    elif i % 5000000 == 0:
        print("Reached turn", i)
    lastNum = nextNum
