# 2020-12-19. Advent of Code 2020, Day 16.
import re
import math

fields = {}
nearbyTickets = []
with open("input.txt", "r") as f:
    for line in f:
        if (m := re.match("(.*): (\d+-\d+)(?: or (\d+-\d+))*", line)) is not None:
            name = m[1]
            validRange = set()
            for i in range(2, len(m.groups())+1):
                lims = m[i].split("-")
                validRange = validRange.union(set(range(int(lims[0]), int(lims[1])+1)))
            fields[name] = validRange
        elif re.match("your ticket", line) is not None:
            s = f.readline().strip().split(",")
            myTicket = [int(i) for i in s]
        elif re.match("[\d,]+", line) is not None:
            s = line.strip().split(",")
            nearbyTickets.append(tuple([int(i) for i in s]))

# Part A
validNums = set().union(*fields.values())
invalidEntries = set([i for tkt in nearbyTickets for i in tkt if i not in validNums])
print("Ticket scanning error rate:", sum(invalidEntries))

# Part B

# remove all the invalid tickets
invalidTickets = []
for t in nearbyTickets:
    if not invalidEntries.isdisjoint(t):
        invalidTickets.append(t)
for t in invalidTickets:
    nearbyTickets.remove(t)

# for each position, identify which fields are potentially valid
fieldPositions = {}
for col in range(len(nearbyTickets[0])):
    fieldPositions[col] = set(fields.keys())
    for t in nearbyTickets:
        for f in fields:
            if t[col] not in fields[f]:
                fieldPositions[col].remove(f)
                break

# assign fields for which there is only one possibility and remove those from other position possibilities until done
while sum([len(i) for i in fieldPositions.values()]) > len(fieldPositions):
    toRemove = []
    for f in fieldPositions:
        if len(fieldPositions[f]) == 1:
            toRemove.append(tuple(fieldPositions[f])[0])
    for r in toRemove:
        for f in fieldPositions:
            if len(fieldPositions[f]) != 1:
                fieldPositions[f].discard(r)

departurePositions = [n for n in fieldPositions if re.match("departure", tuple(fieldPositions[n])[0]) is not None]
print("Positions with 'departure' in the name:", departurePositions)
print("Product of those fields on my ticket:", math.prod([myTicket[i] for i in departurePositions]))
