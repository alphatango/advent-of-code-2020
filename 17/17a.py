# 2020-12-20. Advent of Code 2020, Day 17, Part A


class ConwayCube:
    def __init__(self):
        self.activeCubes = set()
        self.cubeNeighbours = {}
        self.activeCubeNeighbours = set()
        with open("input.txt", "r") as f:
            row = 0
            for line in f:
                col = 0
                for c in line.strip():
                    if c == "#":
                        self.activeCubes.add((row, col, 0))
                    col += 1
                row += 1
            for c in self.activeCubes:
                self.cubeNeighbours[c] = set(self.get_neighbours(c))
                self.activeCubeNeighbours.update(self.cubeNeighbours[c])

    @staticmethod
    def get_neighbours(pos):
        lst = [(pos[0] + x, pos[1] + y, pos[2] + z) for x in (-1, 0, 1) for y in (-1, 0, 1)
               for z in (-1, 0, 1)]
        lst.remove(pos)
        return lst

    def get_adj_active_cubes(self, pos):
        if pos not in self.cubeNeighbours:
            self.cubeNeighbours[pos] = set(self.get_neighbours(pos))
        adjActiveCubes = self.cubeNeighbours[pos].intersection(self.activeCubes)
        return len(adjActiveCubes)

    def evolve(self):
        newActiveCubes = set()
        toCheck = self.activeCubeNeighbours.union(self.activeCubes)
        for c in toCheck:
            if c in self.activeCubes and 2 <= self.get_adj_active_cubes(c) <= 3:
                newActiveCubes.add(c)
            elif c not in self.activeCubes and self.get_adj_active_cubes(c) == 3:
                newActiveCubes.add(c)
        self.activeCubes = newActiveCubes
        self.activeCubeNeighbours = set()
        for c in self.activeCubes:
            self.activeCubeNeighbours.update(self.cubeNeighbours[c])

    def get_num_active_cubes(self):
        return len(self.activeCubes)


conwayCube = ConwayCube()
print("Number of initial active cubes:", conwayCube.get_num_active_cubes())
for i in range(6):
    conwayCube.evolve()
    print("NUmber of active cubes after cycle", i + 1, ":", conwayCube.get_num_active_cubes())
