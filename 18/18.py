# 2020-12-20. Advent of Code 2020, Day 18
import re


class NewMath(int):
    # redefine operators:
    # use "-" to mean multiplication, and "+" and "*" to mean addition
    # then replace the symbols appropriately in the string so that the required order of precedence is achieved
    def __sub__(self, n):
        return NewMath(int(self) * n)

    def __mul__(self, n):
        return NewMath(int(self) + n)

    def __add__(self, n):
        return NewMath(int(self) + n)


def calc_with_equal_precedence(expr):
    expr = re.sub("(\\d+)", "NewMath(\\1)", expr)
    expr = expr.replace("*", "-")  # now addition and multiplication have the same order of precedence
    return eval(expr, {}, {"NewMath": NewMath})


def calc_with_addition_precedence(expr):
    expr = re.sub("(\\d+)", "NewMath(\\1)", expr)
    expr = expr.replace("*", "-")
    expr = expr.replace("+", "*")
    return eval(expr, {}, {"NewMath": NewMath})


with open("input.txt", "r") as f:
    value1 = value2 = 0
    for line in f:
        # print(calc_with_equal_precedence(line))
        # print(calc_with_addition_precedence(line))
        value1 += calc_with_equal_precedence(line)
        value2 += calc_with_addition_precedence(line)

print("Sum of values with addition and multiplication having equal precedence:", value1)
print("Sum of values with addition having precedence over multiplication:", value2)
