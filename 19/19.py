# 2020-12-20. Advent of Code 2020, Day 19.
import re

rules = {}
strings = []
with open("input.txt", "r") as f:
    for line in f:
        if (m := re.match("(\d+)(?:: )(.*)(?:\\n)*", line)) is not None:
            rules[int(m[1])] = m[2]
            if m[2][0] == "\"":
                rules[int(m[1])] = m[2][1:-1]
        elif line != "\n":
            strings.append(line.strip())
print("Rules:", rules, "\n")


def resolve_rule_to_regex(n=0):
    while re.search("\d+", rules[n]) is not None:
        rules[n] = re.sub("\d+", lambda x: "".join(["(", rules[int(x.group(0))], ")"]), rules[n])
    rules[n] = rules[n].replace(" ", "")
    # rules[n] = re.sub("\((a|b)+\)", "\\1", rules[n])
    rules[n] = rules[n].replace("(a)", "a")
    rules[n] = rules[n].replace("(b)", "b")


# Part A
resolve_rule_to_regex(n=0)
count = 0
for s in strings:
    if re.fullmatch(rules[0], s) is not None:
        count += 1
print("Number of strings matching simple case:", count)

# Part B
# By inspection:
# -- Rule 0 = Rule 8 + Rule 11
# -- Rule 8 = 42 | 42 8 --> (Rule 42)+
# -- Rule 11 = 42 31 | 42 11 31 --> n >= 1 repetitions of Rule 42 followed by n repetitions of Rule 31
# Assume that n in Rule 11 is less than 10 and explicitly construct the regex
resolve_rule_to_regex(n=31)
resolve_rule_to_regex(n=42)
rules[8] = "(" + rules[42] + ")+"
rules[11] = "(" + "|".join([f"({rules[42]}){{{n}}}({rules[31]}){{{n}}}" for n in range(1, 10)]) + ")"
rules[0] = rules[8] + rules[11]
count = 0
for s in strings:
    if re.fullmatch(rules[0], s) is not None:
        count += 1
print("Number of strings matching complex case", count)
