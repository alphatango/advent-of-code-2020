# 2020-12-20. Advent of Code 2020, Day 20.
import itertools
import math


class Puzzle:
    def __init__(self):
        self.tiles = set()
        with open("input.txt", "r") as f:
            lines = f.read().split("\n\n")
            tileLines = [s.split("\n") for s in lines]
            for t in tileLines:
                tileNum = int(t[0][-5:-1])
                self.tiles.add(Tile(tileNum, t[1:]))
        self.size = int(math.sqrt(len(self.tiles)))
        self.set_alignments()
        self.cornerTiles = [t for t in self.alignments if len(self.alignments[t]) == 2]
        self.edgeTiles = [t for t in self.alignments if len(self.alignments[t]) == 3]
        self.tilePositions = {}
        self.set_edge_dictionary()

    def set_alignments(self):
        self.alignments = {tile: set() for tile in self.tiles}
        for pair in itertools.combinations(self.tiles, 2):
            if pair[0].can_align_on_edge(pair[1]):
                self.alignments[pair[0]].add(pair[1])
                self.alignments[pair[1]].add(pair[0])

    def set_edge_dictionary(self):
        self.edgeDict = {}
        for t in self.tiles:
            for d in "NESW":
                e = t.get_edge(d)
                if e in self.edgeDict:
                    self.edgeDict[e] += 1
                elif e[::-1] in self.edgeDict:
                    self.edgeDict[e[::-1]] += 1
                else:
                    self.edgeDict[e] = 1

    @staticmethod
    def get_neighbours(pos):
        return {(pos[0] + 1, pos[1]),
                (pos[0] - 1, pos[1]),
                (pos[0], pos[1] + 1),
                (pos[0], pos[1] - 1)
                }

    @staticmethod
    def get_coords_in_dir(pos, c):
        if c == "N":
            return pos[0], pos[1] + 1
        elif c == "E":
            return pos[0] + 1, pos[1]
        elif c == "S":
            return pos[0], pos[1] - 1
        elif c == "W":
            return pos[0] - 1, pos[1]

    @staticmethod
    def get_direction_from_old_to_new_position(oldTilePos, newTilePos):
        diff = (newTilePos[0] - oldTilePos[0], newTilePos[1] - oldTilePos[1])
        d = {(0, 1): "E",
             (0, -1): "W",
             (1, 0): "S",
             (-1, 0): "N"}
        return d[diff]

    def get_outside_edge_directions(self, tile):
        dirs = set()
        for d in "NESW":
            e = tile.get_edge(d)
            if e in self.edgeDict:
                if self.edgeDict[e] == 1:
                    dirs.add(d)
            elif e[::-1] in self.edgeDict:
                if self.edgeDict[e[::-1]] == 1:
                    dirs.add(d)
        return dirs

    def rotate_edge_or_corner_tile_to_match_outside(self, tile, directionsOfOutside):
        while self.get_outside_edge_directions(tile) != directionsOfOutside:
            tile.rotate(cwRot=1)

    def place_tile_using_two_other_tiles(self, pos1, pos2):
        newPos = list(self.get_neighbours(pos1).intersection(self.get_neighbours(pos2)) - self.tilePositions.keys())[0]
        newTile = list(self.alignments[self.tilePositions[pos1]].intersection(self.alignments[self.tilePositions[pos2]])
                       - set(self.tilePositions.values()))[0]
        self.tilePositions[newPos] = newTile
        d = self.get_direction_from_old_to_new_position(pos1, newPos)
        for i in range(4):
            if self.do_edges_match(self.tilePositions[pos1], d, newTile):
                return True
            else:
                newTile.rotate()
        newTile.flip_vertical_axis()
        for i in range(4):
            if self.do_edges_match(self.tilePositions[pos1], d, newTile):
                return True
            else:
                newTile.rotate()
        print("Could not match")

    def do_edges_match(self, tile1, direction, tile2):
        m = {"N": "S",
             "E": "W",
             "S": "N",
             "W": "E"}
        if tile1.get_edge(direction) == tile2.get_edge(m[direction])[::-1]:
            return True
        return False

    def solve(self):
        c = self.cornerTiles[0]
        self.tilePositions = {(0, 0): c}
        self.rotate_edge_or_corner_tile_to_match_outside(c, {"N", "W"})
        for candidate in self.alignments[c]:
            self.rotate_edge_or_corner_tile_to_match_outside(candidate, {"W"})
            if self.do_edges_match(c, "S", candidate):
                self.tilePositions[(1, 0)] = candidate
                break
            else:
                candidate.flip_vertical_axis()
                self.rotate_edge_or_corner_tile_to_match_outside(candidate, {"W"})
                if self.do_edges_match(c, "S", candidate):
                    self.tilePositions[(1, 0)] = candidate
                    break
        self.solve_edge()
        while len(self.tilePositions) != self.size * self.size:
            for i in range(self.size):
                for j in range(self.size):
                    if (i, j) not in self.tilePositions:
                        placedNeighbours = list(self.get_neighbours((i, j)).intersection(self.tilePositions.keys()))
                        if len(placedNeighbours) >= 2:
                            self.place_tile_using_two_other_tiles(placedNeighbours[0], placedNeighbours[1])

    def place_edge_or_corner_tile(self, oldPos, newPos, newTile, outsideEdge):
        self.rotate_edge_or_corner_tile_to_match_outside(newTile, outsideEdge)
        if self.do_edges_match(self.tilePositions[oldPos],
                               self.get_direction_from_old_to_new_position(oldPos, newPos), newTile):
            self.tilePositions[newPos] = newTile
        else:
            newTile.flip_vertical_axis()
            self.rotate_edge_or_corner_tile_to_match_outside(newTile, outsideEdge)
            if self.do_edges_match(self.tilePositions[oldPos],
                                   self.get_direction_from_old_to_new_position(oldPos, newPos), newTile):
                self.tilePositions[newPos] = newTile

    def solve_edge(self):
        for i in range(self.size):
            if (i, 0) not in self.tilePositions:
                candidateTiles = list(self.alignments[self.tilePositions[(i - 1, 0)]].intersection(
                    self.edgeTiles + self.cornerTiles) - set(self.tilePositions.values()))
                candidate = list(candidateTiles)[0]
                if candidate in self.edgeTiles:
                    self.place_edge_or_corner_tile((i - 1, 0), (i, 0), candidate, {"W"})
                elif candidate in self.cornerTiles:
                    self.place_edge_or_corner_tile((i-1, 0), (i, 0), candidate, {"W", "S"})

        for i in range(self.size):
            if (self.size - 1, i) not in self.tilePositions:
                candidateTiles = list(self.alignments[self.tilePositions[(self.size - 1, i - 1)]].intersection(
                    self.edgeTiles + self.cornerTiles) - set(self.tilePositions.values()))
                candidate = list(candidateTiles)[0]
                if candidate in self.edgeTiles:
                    self.place_edge_or_corner_tile((self.size - 1, i - 1), (self.size - 1, i), candidate, {"S"})
                elif candidate in self.cornerTiles:
                    self.place_edge_or_corner_tile((self.size - 1, i - 1), (self.size - 1, i), candidate, {"E", "S"})

        for i in range(self.size - 1, -1, -1):
            if (i, self.size - 1) not in self.tilePositions:
                candidateTiles = list(self.alignments[self.tilePositions[(i + 1, self.size - 1)]].intersection(
                    self.edgeTiles + self.cornerTiles) - set(self.tilePositions.values()))
                candidate = list(candidateTiles)[0]
                if candidate in self.edgeTiles:
                    self.place_edge_or_corner_tile((i + 1, self.size - 1), (i, self.size - 1), candidate, {"E"})
                elif candidate in self.cornerTiles:
                    self.place_edge_or_corner_tile((i + 1, self.size - 1), (i, self.size - 1), candidate, {"E", "N"})

        for i in range(self.size - 1, -1, -1):
            if (0, i) not in self.tilePositions:
                candidateTiles = list(self.alignments[self.tilePositions[(0, i + 1)]].intersection(
                    self.edgeTiles + self.cornerTiles) - set(self.tilePositions.values()))
                candidate = list(candidateTiles)[0]
                if candidate in self.edgeTiles:
                    self.place_edge_or_corner_tile((0, i + 1), (0, i), candidate, {"N"})
                elif candidate in self.cornerTiles:
                    self.place_edge_or_corner_tile((0, i+1), (0, i), candidate, {"N", "W"})

    def combine_tiles(self):
        self.grid = {}
        for t in self.tilePositions.values():
            t.strip_borders()
        for pos, tile in self.tilePositions.items():
            for row, col in itertools.product(range(tile.size - 2), repeat=2):
                self.grid[(row + pos[0] * (tile.size - 2), col + pos[1] * (tile.size - 2))] = tile.grid[row, col]
        self.gridSize = int(math.sqrt(len(self.grid)))

    def rotate(self, cwRot=1):
        for i in range(cwRot):
            newGrid = {}
            for c in self.grid:
                newGrid[(c[1], self.gridSize - c[0] - 1)] = self.grid[c]
            self.grid = newGrid

    def flip_vertical_axis(self):
        newGrid = {}
        for c in self.grid:
            newGrid[(c[0], self.gridSize - c[1] - 1)] = self.grid[c]
        self.grid = newGrid

    def show(self):
        s = ""
        size = int(math.sqrt(len(self.grid)))
        print("Size:", size)
        for row in range(size):
            for col in range(size):
                s += self.grid[(row, col)]
            s += "\n"
        print(s)

    def get_hash_marks_not_in_sea_monsters(self):
        seaMonsters = self.find_sea_monsters()
        print("Found", len(seaMonsters), "sea monsters; marking sea monsters at", seaMonsters)
        for m in seaMonsters:
            for pos in self.seaMonster:
                self.grid[(m[0] + pos[0], m[1] + pos[1])] = "X"
        return sum(1 for c in self.grid if self.grid[c] == "#")

    def find_sea_monsters(self):
        for i in range(4):
            sm = self.find_sea_monsters_in_current_orientation()
            if len(sm) > 0:
                return sm
            p.rotate()
        p.flip_vertical_axis()
        for i in range(4):
            sm = self.find_sea_monsters_in_current_orientation()
            if len(sm) > 0:
                return sm
            p.rotate()
        print("Failed to find any sea monsters in any orientation :(")
        assert (False)

    def find_sea_monsters_in_current_orientation(self):
        # sea monster is size 20 in width, size 3 in height
        self.seaMonster = {}
        smHeight = 3
        smWidth = 20
        for pos in [(0, 18),
                    (1, 0), (1, 5), (1, 6), (1, 11), (1, 12), (1, 17), (1, 18), (1, 19),
                    (2, 1), (2, 4), (2, 7), (2, 10), (2, 13), (2, 16)]:
            self.seaMonster[pos] = "X"
        lstSeaMonsters = []
        size = int(math.sqrt(len(self.grid)))
        # check from possible starting positions given size of sea monster
        for row in range(size - smHeight):
            for col in range(size - smWidth):
                if self.is_sea_monster_present((row, col)):
                    lstSeaMonsters.append((row, col))
        return lstSeaMonsters

    def is_sea_monster_present(self, pos):
        for p in self.seaMonster:
            if self.grid[(pos[0] + p[0], pos[1] + p[1])] != "#":
                return False
        return True


class Tile:
    def __init__(self, tileNum, lst):
        self.num = tileNum
        self.grid = {}
        row = 0
        for i in range(len(lst)):
            col = 0
            for j in range(len(lst[i])):
                self.grid[(row, col)] = lst[row][col]
                col += 1
            row += 1
        self.size = row

    def flip_vertical_axis(self):
        newGrid = {}
        for c in self.grid:
            newGrid[(c[0], self.size - c[1] - 1)] = self.grid[c]
            # print(self.grid)
        self.grid = newGrid

    def rotate(self, cwRot=1):
        for i in range(cwRot):
            newGrid = {}
            for c in self.grid:
                newGrid[(c[1], self.size - c[0] - 1)] = self.grid[c]
            self.grid = newGrid

    def strip_borders(self):
        newGrid = {}
        for row in range(self.size - 2):
            for col in range(self.size - 2):
                newGrid[(row, col)] = self.grid[(row + 1, col + 1)]
        self.grid = newGrid

    def can_align_on_edge(self, otherTile):
        for d in ("N", "E", "S", "W"):
            for e in ("N", "E", "S", "W"):
                if self.get_edge(d) == otherTile.get_edge(e) or self.get_edge(d)[::-1] == otherTile.get_edge(e):
                    return True
        return False

    def get_edge(self, d):
        if d == "N":
            lst = [self.grid[(0, col)] for col in range(self.size)]
        elif d == "E":
            lst = [self.grid[(row, self.size - 1)] for row in range(self.size)]
        # read the bottom and left edges in reverse order (right to left and bottom to top) for consistency
        # note this means a flip about the vertical axis reverses the direction of all edges
        elif d == "S":
            lst = [self.grid[(self.size - 1, col)] for col in range(self.size)][::-1]
        elif d == "W":
            lst = [self.grid[(row, 0)] for row in range(self.size)][::-1]
        return "".join(lst)

    def __repr__(self):
        return str(self.num)

    def __mul__(self, other):
        if isinstance(other, self.__class__):
            return self.num * other.num
        else:
            return self.num * other

    def __rmul__(self, otherTile):
        return self.__mul__(otherTile)

    def show(self):
        s = ""
        for row in range(self.size):
            for col in range(self.size):
                s += self.grid[(row, col)]
            s += "\n"
        print(s)


# Part A
p = Puzzle()
print("Part A: There are", len(p.cornerTiles), "potential corner tiles with ID product",
      math.prod(p.cornerTiles), "\n\n")

# Part B
p.solve()
p.combine_tiles()
print("The solved puzzle looks like this:")
p.show()
print("Part B: There are", p.get_hash_marks_not_in_sea_monsters(), "hash marks not in sea monsters")
