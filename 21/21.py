# 2020-12-23. Advent of Code 2020, Day 21.

recipesToAllergens = {}
allergensToIngredients = {}
with open("input.txt", "r") as f:
    for line in f:
        line = line.strip().split(" (contains ")
        ingredientsStr = line[0]
        ingredients = set(line[0].split())
        allergens = set(line[1][:-1].split(", "))
        recipesToAllergens[ingredientsStr] = allergens
        for a in allergens:
            if a not in allergensToIngredients:
                allergensToIngredients[a] = ingredients.copy()
            else:
                allergensToIngredients[a].intersection_update(ingredients)
allAllergens = set().union(*recipesToAllergens.values())
combinedRecipes = [item for r in recipesToAllergens for item in r.split()]

# Part A
# remove any uniquely identified ingredient-allergen combinations from the possibilities for the other allergens
changed = True
while changed:
    changed = False
    allergensKnown = set([a for a in allAllergens if len(allergensToIngredients[a]) == 1])
    for a in allergensKnown:
        for b in allAllergens-allergensKnown:
            allergensToIngredients[b].discard(list(allergensToIngredients[a])[0])
            if len(allergensToIngredients[b]) == 1:
                changed = True
ingredientsWithAllergens = set().union(*allergensToIngredients.values())
# count them up
count = 0
for safeIng in (set(combinedRecipes) - ingredientsWithAllergens):
    for ing in combinedRecipes:
        if safeIng == ing:
            count += 1
print("Number of times safe ingredients appear:", count)

# Part B
canonicalList = ",".join([list(allergensToIngredients[a])[0] for a in sorted(allergensToIngredients.keys())])
print("Canonical dangerous ingredient list:", canonicalList)
