# 2020-12-22. Advent of Code 2020, Day 22.


class SimpleGame:
    def __init__(self):
        allCards = []
        with open("input.txt", "r") as f:
            for line in f:
                if "Player" not in line and line != "\n":
                    allCards.append(int(line.strip()))
        self.deckSize = len(allCards)//2
        self.deck1 = allCards[:self.deckSize]
        self.deck2 = allCards[self.deckSize:]

    def do_turn(self):
        if self.deck1[0] > self.deck2[0]:
            self.deck1 = self.deck1[1:] + self.deck1[:1] + self.deck2[:1]
            self.deck2 = self.deck2[1:]
        elif self.deck2[0] > self.deck1[0]:
            self.deck2 = self.deck2[1:] + self.deck2[:1] + self.deck1[:1]
            self.deck1 = self.deck1[1:]

    def do_game(self):
        while len(self.deck1) > 0 and len(self.deck2) > 0:
            self.do_turn()
            # print("Deck 1:", self.deck1)
            # print("Deck 2:", self.deck2)
        return self.deck1, self.deck2

    def get_score(self):
        m = list(range(self.deckSize * 2, 0, -1))
        if len(self.deck1) > 0:
            p = [x * y for x, y in zip(self.deck1, m)]
        elif len(self.deck2) > 0:
            p = [x * y for x, y in zip(self.deck2, m)]
        return sum(p)


g = SimpleGame()
g.do_game()
print("Score:", g.get_score())
