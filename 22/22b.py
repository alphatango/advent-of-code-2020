# 2020-12-22. Advent of Code 2020, Day 22.


class RecursiveGame:
    def __init__(self, deck1=None, deck2=None):
        if deck1 is not None and deck2 is not None:
            self.deck1 = deck1
            self.deck2 = deck2
        else:
            allCards = []
            with open("input.txt", "r") as f:
                for line in f:
                    if "Player" not in line and line != "\n":
                        allCards.append(int(line.strip()))
            self.deck1 = allCards[:len(allCards)//2]
            self.deck2 = allCards[len(allCards)//2:]
        self.configs = set()

    def do_turn(self):
        if len(self.deck1) - 1 >= self.deck1[0] and len(self.deck2) - 1 >= self.deck2[0]:
            # both players have enough cards left; play a subgame with next n cards, where n is the value of the
            # card drawn
            # print("Creating subgame with decks", self.deck1[1:1+self.deck1[0]], self.deck2[1:1+self.deck2[0]])
            g = RecursiveGame(deck1=self.deck1[1:1+self.deck1[0]], deck2=self.deck2[1:1+self.deck2[0]])
            if g.do_game() == 1:
                self.deck1 = self.deck1[1:] + self.deck1[:1] + self.deck2[:1]
                self.deck2 = self.deck2[1:]
            else:
                self.deck2 = self.deck2[1:] + self.deck2[:1] + self.deck1[:1]
                self.deck1 = self.deck1[1:]
        else:
            # someone does not have enough cards left; higher value card wins
            if self.deck1[0] > self.deck2[0]:
                self.deck1 = self.deck1[1:] + self.deck1[:1] + self.deck2[:1]
                self.deck2 = self.deck2[1:]
            else:
                self.deck2 = self.deck2[1:] + self.deck2[:1] + self.deck1[:1]
                self.deck1 = self.deck1[1:]
        # print("Deck 1:", self.deck1)
        # print("Deck 2:", self.deck2)

    def do_game(self):
        while len(self.deck1) > 0 and len(self.deck2) > 0:
            if (tuple(self.deck1), tuple(self.deck2)) in self.configs:
                return 1  # Player 1 wins if position is repeated
            else:
                self.configs.add((tuple(self.deck1), tuple(self.deck2)))
            self.do_turn()
        if len(self.deck2) == 0:
            # print("Player 1 wins; going back up")
            return 1
        else:
            # print("Player 2 wins; going back up")
            return 2
            # print("Deck 1:", self.deck1)
            # print("Deck 2:", self.deck2)

    def get_score(self):
        m = list(range(len(self.deck1)+len(self.deck2), 0, -1))
        if len(self.deck1) > 0:
            p = [x * y for x, y in zip(self.deck1, m)]
        elif len(self.deck2) > 0:
            p = [x * y for x, y in zip(self.deck2, m)]
        return sum(p)


g = RecursiveGame()
winner = g.do_game()
print("Winner is Player", winner, "with score:", g.get_score())
