# 2020-12-23. Advent of Code 2020, Day 23.


class Cup:
    def __init__(self, num=None):
        self.num = num
        self.nextCup = None
        self.lastCup = None

    def __repr__(self):
        return str(self.num)


class CupCircle:
    def __init__(self):
        self.currentCup = None
        with open("input.txt", "r") as f:
            line = f.readline().strip()
            self.cups = [None]*len(line)
            for c in line:
                newCup = Cup(num=int(c))
                self.cups[newCup.num-1] = newCup
                if self.currentCup is None:
                    firstCup = newCup
                else:
                    self.currentCup.nextCup = newCup
                    newCup.lastCup = self.currentCup
                self.currentCup = newCup
            self.currentCup.nextCup = firstCup
            firstCup.lastCup = self.currentCup
            self.currentCup = firstCup

    def get_num_cups(self):
        return len(self.cups)

    def get_cup(self, num):
        return self.cups[num-1]

    def get_next_cups(self, startingCup, numCups=1):
        lst = [startingCup.nextCup]
        for i in range(1, numCups):
            lst.append(lst[-1].nextCup)
        return lst

    def move_cup(self, after, cupToInsert):
        cupToInsert.lastCup.nextCup = cupToInsert.nextCup
        cupToInsert.nextCup.lastCup = cupToInsert.lastCup
        cupToInsert.nextCup = after.nextCup
        after.nextCup = cupToInsert
        cupToInsert.nextCup.lastCup = cupToInsert
        cupToInsert.lastCup = after

    def insert_cup(self, after, newCup):
        newCup.nextCup = after.nextCup
        newCup.lastCup = after
        after.nextCup.lastCup = newCup
        after.nextCup = newCup

    def move_cup_chain(self, after, firstCupToInsert, lastCupToInsert):
        firstCupToInsert.lastCup.nextCup = lastCupToInsert.nextCup
        lastCupToInsert.nextCup.lastCup = firstCupToInsert.lastCup
        after.nextCup.lastCup = lastCupToInsert
        lastCupToInsert.nextCup = after.nextCup
        after.nextCup = firstCupToInsert
        firstCupToInsert.lastCup = after

    def move(self):
        cupsExtracted = self.get_next_cups(self.currentCup, numCups=3)
        destCupNum = (self.currentCup.num - 2) % self.get_num_cups() + 1
        while self.get_cup(destCupNum) in cupsExtracted:
            destCupNum = (destCupNum-2) % self.get_num_cups() + 1
        self.move_cup_chain(self.get_cup(destCupNum), cupsExtracted[0], cupsExtracted[-1])
        self.currentCup = self.currentCup.nextCup

    def __repr__(self):
        s = str(self.currentCup.num)
        c = self.currentCup.nextCup
        while c is not self.currentCup:
            s += str(c.num)
            c = c.nextCup
        return s

    def get_cups_after_cupNum1(self, numCups=2):
        lst = []
        c = self.get_cup(1)
        for i in range(numCups):
            c = c.nextCup
            lst.append(c)
        return lst


# Part A
print("Part A")
circle = CupCircle()
for i in range(10):
    # print(circle)
    circle.move()

print("List of cups after cup 1:", circle.get_cups_after_cupNum1(numCups=(circle.get_num_cups()-1)))
#
# Part B
print("Part B")
circle2 = CupCircle()
for i in range(10,1000001):
    c = Cup(i)
    circle2.cups.append(c)
    circle2.insert_cup(circle2.currentCup.lastCup, c)
print("Inserted cup", i)

for i in range(10000000):
    circle2.move()
    if (i+1) % 500000 == 0:
        print("Did move", i + 1)
        print("Current five cups after cup 1:", circle2.get_cups_after_cupNum1(numCups=5))
nextTwoCups = circle2.get_cups_after_cupNum1(numCups=2)
print("Next cups are:", nextTwoCups)
print("Their product is:", nextTwoCups[0].num * nextTwoCups[1].num)
