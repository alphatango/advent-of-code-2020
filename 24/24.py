# 2020-12-24. Advent of Code 2020, Day 24.
import re


class HexGrid:
    # axial coords: x=0 runs NW-SE, y=0 runs W-E
    # True = white, False = black
    def __init__(self):
        self.blackHexes = set()
        self.neighbours = {}
        self.pos = (0, 0)
        with open("input.txt", "r") as f:
            self.inst = f.read().strip().split("\n")
        for n in range(len(self.inst)):
            self.inst[n] = re.findall("nw|ne|se|sw|e|w", self.inst[n])

    def move(self, d):
        if d == "nw":
            self.pos = (self.pos[0], self.pos[1] - 1)
        elif d == "ne":
            self.pos = (self.pos[0] + 1, self.pos[1] - 1)
        elif d == "sw":
            self.pos = (self.pos[0] - 1, self.pos[1] + 1)
        elif d == "se":
            self.pos = (self.pos[0], self.pos[1] + 1)
        elif d == "e":
            self.pos = (self.pos[0] + 1, self.pos[1])
        elif d == "w":
            self.pos = (self.pos[0] - 1, self.pos[1])

    def do_instruction(self, lst):
        self.pos = (0, 0)
        for d in lst:
            self.move(d)
        if self.pos not in self.blackHexes:
            self.blackHexes.add(self.pos)
        else:
            self.blackHexes.remove(self.pos)

    def do_instructions(self, lst):
        for instruction in lst:
            self.do_instruction(instruction)

    def get_num_black_tiles(self):
        return len(self.blackHexes)

    @staticmethod
    def get_neighbours(cell):
        return {(cell[0], cell[1] - 1),
                (cell[0], cell[1] + 1),
                (cell[0] + 1, cell[1] - 1),
                (cell[0] - 1, cell[1] + 1),
                (cell[0] + 1, cell[1]),
                (cell[0] - 1, cell[1])
                }

    def get_num_black_neighbours(self, cell):
        if cell not in self.neighbours:
            self.neighbours[cell] = self.get_neighbours(cell)
        return len([c for c in self.neighbours[cell] if c in self.blackHexes])

    def get_neighbours_of_set(self, setCells):
        neighbours = set()
        for c in setCells:
            if c not in self.neighbours:
                self.neighbours[c] = self.get_neighbours(c)
            neighbours.update(self.neighbours[c])
        return neighbours

    def do_flips(self):
        newBlackHexes = set()
        neighbourWhiteHexes = self.get_neighbours_of_set(self.blackHexes) - self.blackHexes
        for h in self.blackHexes:
            if 1 <= self.get_num_black_neighbours(h) <= 2:
                newBlackHexes.add(h)
        for h in neighbourWhiteHexes:
            if self.get_num_black_neighbours(h) == 2:
                newBlackHexes.add(h)
        self.blackHexes = newBlackHexes


# Part A
grid = HexGrid()
grid.do_instructions(grid.inst)
print("Number of black tiles after executing instructions:", grid.get_num_black_tiles())

# Part B
for i in range(100):
    grid.do_flips()
print("Number of black tiles after day 100:", grid.get_num_black_tiles())
