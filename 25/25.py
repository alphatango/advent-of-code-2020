# 2020-12-25. Advent of Code 2020, Day 25.
import re

with open("input.txt", "r") as f:
    publicKeys = [int(c) for c in f.read().strip().split("\n")]
    print("Public keys:", publicKeys)

def transform(val=1, subjNum=7):
    val = val * subjNum % 20201227
    return val

def get_loop_sizes():
    loopsRequired = [None, None]
    i = 1
    val = 7
    while True:
        i+=1
        val = transform(val=val, subjNum=7)
        if val == publicKeys[0]:
            loopsRequired[0] = i
        if val == publicKeys[1]:
            loopsRequired[1] = i
        if None not in loopsRequired:
            return loopsRequired

loopSizes = get_loop_sizes()
print("Loop sizes:",loopSizes)
encryption = 1
for i in range(loopSizes[1]):
    encryption = transform(val=encryption, subjNum=publicKeys[0])
print("Encryption:", encryption)
